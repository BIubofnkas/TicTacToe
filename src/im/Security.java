package im;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

/**
 *
 * @author puser
 */
public class Security {
    
/////////////////////////////////
//AES////////////////////////////
//////////////////////////////////

//generate AES key
 public static Key getAESKey() throws Exception
 {
    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    keyGen.init(128);
    Key key = keyGen.generateKey();
    return key;
 } 


 //encrypt message m with symmetric key k with AES algorithm
 public String encryptAES(String message, Key key) throws Exception
 {
     //this doesn't need to be re declared every time
     //can declare it in main later on.
     byte[] msg = message.getBytes("UTF8");
     Cipher cipher = Cipher.getInstance("AES");
     cipher.init(Cipher.ENCRYPT_MODE, key);     
     byte[] cipherText = cipher.doFinal(msg);
     return new String(cipherText, "UTF8");
 
 }


 //encrypt ciphertext c with symmetric key k using AES algorithm
  public String decryptAES(String cipherText, Key key) throws Exception
 {
     //same as encrypt mostly
     byte[] msg = cipherText.getBytes("UTF8");
     Cipher cipher = Cipher.getInstance("AES");
     cipher.init(Cipher.DECRYPT_MODE, key);     
     byte[] plainText = cipher.doFinal(msg);
     return new String(plainText, "UTF8");
 
 }

//////////////////////////////////
//SHA-2//////////////////////////
//////////////////////////////////
//gets an SHA-2 Hash of the input text
public String SHA_2(String message) throws Exception 
{
    byte[] plainText = message.getBytes("UTF8");
    MessageDigest hash = MessageDigest.getInstance("SHA-256");
    hash.update(plainText);
    return new String(plainText, "UTF8");
}

//////////////////////////////////
//RSA////////////////////////////
//////////////////////////////////
  public KeyPair generateRSAKey() throws Exception
    {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(1024);
        return keyGen.generateKeyPair();   
    }
    //generates an RSA encryption
    //key should be public or private key of an RSA key pair
    //public for encryption private for signing
    //signing will probably be encrypting a SHA-2 of message with private key
    //can just input the hash and private key into this function
    //this will probably only be used for symmetry key transmission
    public String encryptRSA(String message, Key key) throws Exception
    {
        byte[] msg = message.getBytes("UTF8");
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] cipherText = cipher.doFinal(msg);
        return new String(cipherText,"UTF8");
    }
    public String decryptRSA(String cipherText, Key key) throws Exception
    {
        byte[] msg = cipherText.getBytes("UTF8");
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plainText = cipher.doFinal(msg);
        return new String(plainText,"UTF8");
    }


public static void RSA_hard(String message) throws Exception {
    
    // get an MD5 message digest object and compute the plaintext diges
    System.out.println( "\n Message in: " );
    System.out.printf("%s\n", message);
    byte[] msg = message.getBytes("UTF8");
    // generate an RSA keypair
    System.out.println( "\nStart generating RSA key" );
    KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
    keyGen.initialize(1024);
    KeyPair key = keyGen.generateKeyPair();
    System.out.println( "Finish generating RSA key" );
    Key pbkey = key.getPublic();
    Key pvkey = key.getPrivate();
    String str_pbkey = new String(Base64.getEncoder().encode(pbkey.getEncoded()));
    String str_pvkey = new String(Base64.getEncoder().encode(pvkey.getEncoded()));
    System.out.println("Key generated:");
    System.out.printf("public: %s \n private: %s",str_pbkey, str_pvkey);
    //
    // get an RSA cipher and list the provider
    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
    System.out.println( "\n" + cipher.getProvider().getInfo() );
    //
    // encrypt the message digest with the RSA private key
    // to create the signature
    System.out.println( "\nStart encryption" );
    cipher.init(Cipher.ENCRYPT_MODE, key.getPrivate());
    byte[] cipherText = cipher.doFinal(msg);
    System.out.println( "Encrypted message: " );
    System.out.println( new String(cipherText, "UTF8") );
    //
    // to verify, start by decrypting the signature with the
    // RSA private key
    System.out.println( "\nStart decryption" );
    cipher.init(Cipher.DECRYPT_MODE, key.getPublic());
 
    byte[] newmsg = cipher.doFinal(cipherText);
    System.out.println( "Decrypted message: " );
    System.out.println( new String(newmsg, "UTF8") );
    
 }
  public static void AES(String ciphertext) throws Exception
 {
        
    byte[] plainText = ciphertext.getBytes();
    //
    // get a AES private key
    System.out.println( "\nStart generating AES key" );
    KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    keyGen.init(256);
    Key key = keyGen.generateKey();
    System.out.println( "Finish generating AES key" );
    //
    // get a DES cipher object and print the provider
    Cipher cipher = Cipher.getInstance("AES");
    System.out.println( "\n" + cipher.getProvider().getInfo() );
    //
    // encrypt using the key and the plaintext
    String str_key = new String(Base64.getEncoder().encode(key.getEncoded()));
    System.out.println("Key generated:");
    System.out.printf("%s",str_key);
    System.out.println( "\nStart encryption" );
    cipher.init(Cipher.ENCRYPT_MODE, key);
    byte[] cipherText = cipher.doFinal(plainText);
    System.out.println( "Finish encryption: " );
    System.out.println( new String(cipherText, "UTF8") );
 
    //
    // decrypt the ciphertext using the same key
    System.out.println( "\nStart decryption" );
    cipher.init(Cipher.DECRYPT_MODE, key);
    byte[] newPlainText = cipher.doFinal(cipherText);
    System.out.println( "Finish decryption: " );
 
    System.out.println( new String(newPlainText, "UTF8") );
 }


 
 public static void SHA(String message) throws Exception {
 
  byte[] plainText = message.getBytes("UTF8");
  System.out.printf( "\n unhashed msg: %s", message );
  //
  // get a message digest object using the MD5 algorithm
  //SHA-2 
  MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
  //
  // print out the provider used
  System.out.println( "\n" + messageDigest.getProvider().getInfo() );
  //
  // calculate the digest and print it out
  messageDigest.update( plainText);
  System.out.println( "\n hashed msg: " );
  System.out.println( new String( messageDigest.digest(), "UTF8") );
 }
    

}
package im;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class client {

    private boolean listen = true;
    public final String name;
    private Socket socket;
    private ObjectInputStream in;
    private ObjectOutputStream out;
    private MessageGUI Message_screen;



    final public String getName() {
        return name;
    }

    public client(String serverAddress, String string_port, String name, MessageGUI Message_screen) throws Exception {
        try {
            int port = Integer.parseInt(string_port);
            this.Message_screen = Message_screen;

            System.out.println("Connecting to... ");
            System.out.println("serverAddress : " + serverAddress);
            System.out.println("port : " + port);
            System.out.println("name : " + name);
            this.socket = new Socket(serverAddress, port);
            this.name = name;
            System.out.println("Connection Established " + this.socket.getInetAddress() + ":" + this.socket.getPort());
            this.out = new ObjectOutputStream(this.socket.getOutputStream());
            this.out.flush();
            System.out.println("out done");
            this.in = new ObjectInputStream(this.socket.getInputStream());
            System.out.println("in done");
            sendReqToServer(new ChatMessage(ChatMessage.REGISTER, name));

        } catch (IOException ex) {
            throw new Exception("Not able to establish connection to server");
        }
    }


    final public void sendReqToServer(ChatMessage message) {
        try {
            System.out.println("Sending req to server : ");
            System.out.print("Message : " + message + "\n");
            out.writeObject(message);
        } catch (IOException e) {
            System.err.println("Exception writing to server: " + e);
        }
    }

    public void listenFromServer() {
        ArrayList<String> users;
        ChatMessage message;
        byte[] mybytearray;
        String fileLoc;
        FileOutputStream stream;
        while (listen) {
            try {
                message = (ChatMessage) in.readObject();
                System.out.println("Message is: ");
                System.out.println(message);

                switch (message.getType()) {
                    case ChatMessage.USERLIST:
                        System.out.println("Message Type : UserList");
                        users = message.getUsers();
                        for (String username : users) {
                            if (!username.equalsIgnoreCase(this.name)) {
                                Message_screen.addUser(username);
                            }
                        }
                        break;

                    case ChatMessage.DEREGISTER:
                        System.out.println("Message Type : deregister");
                        Message_screen.removeUser(message.getMessage());
                        break;



                    case ChatMessage.FILE:
                        System.out.println("Message Type : File");
                        fileLoc = Message_screen.getFileLocation(message.getFrom());
                        mybytearray = message.getByteArray();
                        System.out.println("storing file : " + fileLoc + "/" + message.getMessage());
                        stream = new FileOutputStream(fileLoc + "/" + message.getMessage());
                        try {
                            stream.write(mybytearray);
                        } finally {
                            stream.close();
                        }
                        Message_screen.writeToChatBox(message);
                        break;



                    case ChatMessage.MESSAGE:
                    case ChatMessage.EMESSAGE:
                        System.out.println("Message Type : Message");

                        Message_screen.writeToChatBox(message);



                }
            } catch (ClassNotFoundException | IOException ex) {
                System.out.println("Exception when listening to server : " + ex);
            } catch (Exception e) {
                System.out.println("Exception when listening to server : " + e);
            }
        }

    }



    public void disconnect() {

        try {
            sendReqToServer(new ChatMessage(ChatMessage.DEREGISTER, name));
            listen = false;
            socket.close();
        } catch (IOException ex) {
            Logger.getLogger(client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
package im;

import javafx.scene.control.Alert;

import java.sql.*;


public class login_mysql {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ResultSet resultSet2 = null;
    private boolean success = false;

    public boolean readDataBase(String username, String entered_password) throws Exception {
        Alert no_sql = new Alert(Alert.AlertType.INFORMATION);

        try {





            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            resultSet2 = statement
                    .executeQuery("select * from users");

            if (view_existing_users(resultSet2, username, entered_password) == true){
                System.out.println("Username and password is correct");
                success = true;
            }
            else{
                System.out.println("Username or password incorrect");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Username or password incorrect");
                alert.showAndWait();
            }


        } catch (Exception e) {
            no_sql.setHeaderText(null);
            no_sql.setContentText("MySQL database not detected");
            no_sql.showAndWait();
            throw e;

        } finally {
            close();
        }
        return success;

    }


    private boolean view_existing_users(ResultSet resultSet, String new_username, String entered_password) throws SQLException {
        // ResultSet is initially before the first data set
        boolean password_matches = false;
        
        while (resultSet.next()) {

            String username = resultSet.getString("username");
            String password = resultSet.getString("password");

            if(username.equals(new_username)){

                if (BCrypt.checkpw(entered_password, password)) {
                    System.out.println("It matches");
                    password_matches = true;
                }
                else
                    System.out.println("It does not match");
            }

        }

        if (password_matches == true){
            return true;
        }
        else{
            return false;
        }
    }



    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }






}
package im;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class get_messages {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public List<String> readDataBase(String user1, String user2) throws Exception {
        List<String> messages;
        try {




            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            preparedStatement = connect
                    .prepareStatement("select * from messages WHERE (sender = ? AND reciever = ?) OR (sender = ? AND reciever = ?)  ");
            preparedStatement.setString(1, user1);
            preparedStatement.setString(2, user2);
            preparedStatement.setString(3, user2);
            preparedStatement.setString(4, user1);


            resultSet = preparedStatement.executeQuery();

            messages = view_users(resultSet);

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
        return messages;
    }



    private List<String> view_users(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        List<String> usernames = new ArrayList<String>();
        while (resultSet.next()) {
            usernames.add(resultSet.getString("sender"));
            usernames.add(resultSet.getString("message"));

        }
        return usernames;

    }



    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }



}
package im;

import javafx.scene.control.Alert;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class register_mysql {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private ResultSet resultSet2 = null;

    public boolean readDataBase(String username, String password) throws Exception {
        boolean success = false;
        try {

            String hashed_password = BCrypt.hashpw(password, BCrypt.gensalt(12));

            System.out.println(hashed_password);


            Class.forName("org.mariadb.jdbc.Driver");

            connect = DriverManager
                    .getConnection("jdbc:mariadb://localhost/feedback?"
                            + "user=sqluser&password=sqluserpw");

            // Statements allow to issue SQL queries to the database
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("USE feedback");

            resultSet2 = statement
                    .executeQuery("select * from users");

            if (view_existing_users(resultSet2, username) == true){
                System.out.println("This is a new username");
                preparedStatement = connect
                        .prepareStatement("insert into users values (default, ?, ?)");

                preparedStatement.setString(1, username);
                preparedStatement.setString(2, hashed_password);


                preparedStatement.executeUpdate();
                success = true;
            }
            else{
                System.out.println("This username exists");
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("This username exists");

                alert.showAndWait();

            }


        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }
        return success;
    }


    private boolean view_existing_users(ResultSet resultSet, String new_username) throws SQLException {
        // ResultSet is initially before the first data set
        boolean same_usernames = false;
        
        while (resultSet.next()) {

            String username = resultSet.getString("username");

            if(username.equals(new_username)){
                same_usernames = true;
            }

        }

        if (same_usernames == true){
            return false;
        }
        else{
            return true;
        }
    }



    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }






}